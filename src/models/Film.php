<?php

class Film
{
    private $id;
    private $title;
    private $realisateur;
    private $desc;
    private $note;
    private $media;
    private $jaquette;
    private $duree;

    public function __construct()
    {
        $this->media = [];
    }

    public function addMedia($med)
    {
        array_push($this->media, $med);
    }

    /**
     * Get the value of title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of realisateur
     */
    public function getRealisateur()
    {
        return $this->realisateur;
    }

    /**
     * Set the value of realisateur
     *
     * @return  self
     */
    public function setRealisateur($realisateur)
    {
        $this->realisateur = $realisateur;

        return $this;
    }

    /**
     * Get the value of desc
     */
    public function getDesc()
    {
        return nl2br($this->desc);
    }

    /**
     * return the 50 first carateres
     */
    public function getExcerp($limit = 50)
    {
        return substr($this->desc, 0, $limit) . "...";
    }
    /**
     * Set the value of desc
     *
     * @return  self
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;

        return $this;
    }

    /**
     * Get the value of note
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set the value of note
     *
     * @return  self
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get the value of jaquette
     */
    public function getJaquette()
    {
        return $this->jaquette;
    }

    /**
     * Set the value of jaquette
     *
     * @return  self
     */
    public function setJaquette($jaquette)
    {
        $this->jaquette = $jaquette;

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of duree
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set the value of duree
     *
     * @return  self
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }
}
